# Mulesoft Learning

## Exam Help

##### 这个是Mulesoft官方提供的题目,里面的答案并不是全部正确，请自己甄别. 可以帮助你拿到考试券
Mule prac exam.docx

##### 这是网上找到的题目，答案并不是全部正确，请自己甄别
mule prac online.docx

##### Dataweave的函数，具体请参考官方文档 
https://docs.mulesoft.com/dataweave/2.4/dw-core

##### 刷题网站，值得参考学习，答案并非完全正确，请自己甄别
https://blog.csdn.net/qinai700/article/details/104954050/

##### 这是网上找到的题目，答案并不是全部正确，请自己甄别
MCD-Level-1.pdf

MuleSoft.MCD-Level-1.v2020-03-17.q36.pdf

MuleSoft.MCD-Level-1.v2021-08-28.q35.pdf

##### 重点关注这份题目，目前来说，对考试很有帮助
MuleSoft.MCD-Level-1.v2022-03-29.q73.pdf

## 特别鸣谢
John，Jack, Van, Ricardzz
